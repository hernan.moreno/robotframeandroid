** Settings ***
Documentation  Simple example using AppiumLibrary
Library  AppiumLibrary

 
*** Variables ***
${APPIUM_SERVER}     http://localhost:4723/wd/hub
${PLATFORM_NAME}    Android
${PLATFORM_VERSION}    7.0
${DEVICE_NAME}    emulator-5554
${ACTIVITY_NAME}        com.allwork.MainActivity
${PACKAGE_NAME}     com.allwork
${APP}     ${EXECDIR}/app/talent.apk
${Automation_name}                     UiAutomator2
${Adb_Exec_Timeout}                    60000

*** Test Cases ***
Login Successfully
    Create Conection
    The user is on the AllWorks homepage


*** Keywords ***
Create Conection   
    Open Application
    ...    ${APPIUM_SERVER}
    ...    platformName=${PLATFORM_NAME} 
    ...    platformVersion=${PLATFORM_VERSION}
    ...    deviceName=${DEVICE_NAME}        
    ...    appActivity=${ACTIVITY_NAME}       
    ...    appPackage=${PACKAGE_NAME}
    ...    app=${APP}
    ...    automationName=${Automation_name}
    ...    adbExecTimeout=${Adb_Exec_Timeout}
    ...    uiautomator2ServerInstallTimeout=50000

The user is on the AllWorks homepage
    Sleep  30s
    Wait Until Element Is Visible      xpath =//*[@resource-id='login-username-test-id']
    Page Should Contain Element        xpath =//*[@resource-id='login-password-test-id']
    Input Text                         xpath =//*[@resource-id='login-username-test-id']        salesrep
    Input Text                         xpath =//*[@resource-id='login-password-test-id']        socket123
    Click Element                      xpath =//*[contains(@text,'LOG IN')]        
    Capture page screenshot
    Sleep  90s